package main

import (
	"context"

	"github.com/sirupsen/logrus"

	"gitlab.com/dima_rogovoi/assessment-task-golang/client"
	"gitlab.com/dima_rogovoi/assessment-task-golang/config"
	"gitlab.com/dima_rogovoi/assessment-task-golang/handlers"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	entry := logrus.NewEntry(logger)

	binanceClient := client.NewBinanceClient(cfg, entry)
	updHandler := handlers.NewUpdatesHandler(entry)

	go updHandler.ProcessUpdates(ctx, binanceClient.GetUpdates())
	err = binanceClient.Start(ctx)
	if err != nil {
		panic(err)
	}
}
