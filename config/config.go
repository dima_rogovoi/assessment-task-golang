package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	ApiKey       string `mapstructure:"API_KEY"`
	SecretKey    string `mapstructure:"SECRET_KEY"`
	SymbolsCount int    `mapstructure:"SYMBOLS_COUNT"`
}

func NewConfig() (*Config, error) {
	viper.SetConfigFile(".env")

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var config Config
	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	return &config, nil
}
