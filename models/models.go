package models

type Ticker struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}
