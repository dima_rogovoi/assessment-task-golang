FROM golang:1.21

WORKDIR /assessment-task-golang

COPY . .

RUN go mod download && go build -o assessment-task-golang

CMD ["./assessment-task-golang"]