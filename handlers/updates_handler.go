package handlers

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
)

type UpdatesHandler struct {
	logger *logrus.Entry
}

func NewUpdatesHandler(logger *logrus.Entry) *UpdatesHandler {
	return &UpdatesHandler{logger: logger}
}

func (h *UpdatesHandler) ProcessUpdates(ctx context.Context, updates chan map[string]string) {
	for {
		select {
		case <-ctx.Done():
			h.logger.Info("updates handler stopped, context is closing")

			return
		case update := <-updates:
			for symbol, price := range update {
				fmt.Printf("%s %s\n", symbol, price)
			}
		default:
		}
	}
}
