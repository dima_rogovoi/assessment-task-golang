package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sync"

	"github.com/aiviaio/go-binance/v2"
	"github.com/sirupsen/logrus"

	"gitlab.com/dima_rogovoi/assessment-task-golang/config"
	"gitlab.com/dima_rogovoi/assessment-task-golang/models"
)

type BinanceClient struct {
	client  *binance.Client
	logger  *logrus.Entry
	cfg     *config.Config
	updates chan map[string]string
}

func NewBinanceClient(cfg *config.Config, logger *logrus.Entry) *BinanceClient {
	return &BinanceClient{
		client:  binance.NewClient(cfg.ApiKey, cfg.SecretKey),
		logger:  logger,
		cfg:     cfg,
		updates: make(chan map[string]string),
	}
}

func (b *BinanceClient) Start(ctx context.Context) error {
	symbols, err := b.getFirstSymbolsPairs(ctx, b.cfg.SymbolsCount)
	if err != nil {
		return err
	}

	wg := new(sync.WaitGroup)
	for _, symbol := range symbols {
		wg.Add(1)
		go b.getLastPrice(wg, symbol)
	}

	wg.Wait()
	b.logger.Info("Binance client finish work")

	return nil
}

func (b *BinanceClient) GetUpdates() chan map[string]string {
	return b.updates
}

func (b *BinanceClient) getFirstSymbolsPairs(ctx context.Context, count int) ([]string, error) {
	res := make([]string, count)

	exchangeInfo, err := b.client.NewExchangeInfoService().Do(ctx)
	if err != nil {
		return nil, err
	}

	for i := 0; i < count; i++ {
		res[i] = exchangeInfo.Symbols[i].Symbol
	}

	return res, nil
}

func (b *BinanceClient) getLastPrice(wg *sync.WaitGroup, symbol string) {
	defer wg.Done()

	url := fmt.Sprintf("%s/api/v3/ticker/price?symbol=%s", b.client.BaseURL, symbol)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	resp, err := b.client.HTTPClient.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var ticker models.Ticker
	err = json.Unmarshal(body, &ticker)
	if err != nil {
		return
	}

	b.updates <- map[string]string{ticker.Symbol: ticker.Price}
}
